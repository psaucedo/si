from flask import Flask, render_template, request, session, redirect, url_for, make_response
import usr 
from mv import get_movies

app = Flask(__name__)
app.secret_key = 'SS'

@app.route('/')
def index():
    if 'username' in session:
      usr = session['username']
    return render_template('index.html', categorias= ["Action", "Adventure", "Sci-Fi"], pelis=get_movies(), usr=usr)

@app.route('/registro', methods = ['POST', 'GET'])
def registro():
    uname = request.form.get('uname')
    psw = request.form.get('psw')
    email = request.form.get('email')
    cc = request.form.get('cc')

    if(uname != None):
        if(usr.register(uname, psw, email, cc) != 0):
            return render_template('registro.html', err="Error, usuario ya existe")
        else: 
            session['username'] = uname
            return redirect(url_for('index'))
            


    return render_template('registro.html')

@app.route('/login', methods = ['POST', 'GET'])
def login():
   uname = request.form.get('uname')
   psw = request.form.get('psw')

   if(uname != None): 
      if(usr.login(uname, psw) != 0):
          #Error
        return render_template('login.html', err="Error, Usuario o Contraseña incorrecta")
      else: 
          #Acierto
         session['username'] = uname
         resp = make_response(redirect(url_for('index'))) 
    
         return resp
  
   return render_template('login.html')


@app.route('/detalle/<id>', methods = ['POST', 'GET'])
def peli(id):
    if 'username' in session:
      usr = session['username']
    results = [t for t in get_movies() if t['imdbID'] == id]
    peli = results[0]
    return render_template('detalle.html', peli=peli, usr=usr)

@app.route('/search', methods = ['POST', 'GET'])
def search():
    keyword= request.args.get('search')
    cat = request.args.get('cat')
    results = [t for t in get_movies() if keyword in t['Title'] and cat in t['Genre']]
    return render_template('index.html', categorias= ["Action", "Adventure", "Sci-Fi"], pelis=results)

@app.route('/addtocart/<id>' , methods = ['POST', 'GET'])
def addtocart(id):
    results = [t for t in get_movies() if t['imdbID'] == id]
    peli = results[0]
    user = session['username']
    if(usr.addToCart(user,peli) != 0):
        return render_template('login.html', err="Error, no se ha añadido al carrito correctamente.")
    else:
        return redirect(url_for('index'))



    

if __name__ == '__main__': 
    app.run(debug=True)