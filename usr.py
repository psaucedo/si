# -*- coding: utf-8 -*-
import json
from random import randint
import os
import hashlib
md5= hashlib.md5


def login(uname, pswX):
    rUser = "usuarios/" + uname
    if(not os.path.isdir(rUser)):
        return -1
    rData = rUser + "/datos.dat"

    file = open(rData, "r")
    for line in file:
        if(line.find("Psw:") != -1): 
            psw = line
            break
    file.close()

    psw = psw.replace("\n", "")
    psw = psw.replace("Psw:", "")
    pswX = str(hashlib.md5(pswX.encode('utf-8')).digest() )
    if(pswX != psw):
        return -1



    return 0

def register(uname, psw, email, cc):
   rUser = "usuarios/" + uname
   if(os.path.isdir(rUser)): 
      return -1
  
   print("Making directory...")
   os.mkdir(rUser)
   
   rData = rUser + "/datos.dat"
   file = open(rData, "w")
   file.write("Uname:" + uname +"\n")
   sec = hashlib.md5(psw.encode('utf-8')).digest()
   file.write("Psw:" + str(sec) + "\n")
   file.write("Mail:" + email + "\n")
   file.write("CC:" + cc + "\n")
   credit = randint(0, 100)
   file.write("Balance:" + str(credit) + "\n")
   file.close()
   print("creating JSON...")

   rJSON = rUser + "/historial.json"
   fJSON = open(rJSON, "w+", encoding='utf-8')
   fJSON.close()
   print("Finished creating JSON")

   """Parte del carrito """
   rCart = rUser + "/cart.json"

   cartFile = open(rCart, "w")
   data = dict(Peliscarrito= [])
   json.dump(data, cartFile)
   cartFile.close()

   return 0



def addToCart(usr,peli):
    rUser = "usuarios/" + usr
    if (not os.path.isdir(rUser)):
        return -1

    rCart = rUser + "/cart.json"
    file = open(rCart, "w")
    diccionario = json.(file)
    peliCarrito = dict(Titulo = peli['Title'], Precio = peli['Price'] )
    diccionario["Peliscarrito"].append(peliCarrito)
    file.truncate(0)
    json.dump(diccionario,file)
    file.close()
    print("Carrito añadido correctamente")
    return 0

def viewCart(usr):
    rUser = "usuarios/" + usr
    if (not os.path.isdir(rUser)):
        return -1
    rCart = rUser + "/cart.json"
    file = open(rCart)
    data = json.load(file)





